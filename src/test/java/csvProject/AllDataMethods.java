package csvProject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;



public class AllDataMethods {

	public static void main(String[] args) throws IOException, ParseException {
		FTPClient ftp = new FTPClient();
		ftp.connect("54.229.200.159");
		ftp.login("Autovit.ro", "autovit9123");
		ftp.enterLocalPassiveMode();
		ftp.setFileType(FTP.BINARY_FILE_TYPE);
		LocalDate now = LocalDate.now().minusDays(1);
		String date = "" + now.getYear() + now.getMonthValue() + (now.getDayOfMonth());
		System.out.println(date);

		String Path = date + " - AVT - AVT Tradus ads export.csv";
		String file = "C:\\Users\\K Vinay\\Desktop\\Json files\\" + date + " - AVT - AVT Tradus ads export.csv";
		FileOutputStream fos = new FileOutputStream(new File(file));
		FileReader fileReader = new FileReader(file);
		ftp.retrieveFile(Path, fos);
		CSVReader reader = new CSVReaderBuilder(fileReader).withSkipLines(1).build();
		List<String[]> list = reader.readAll();
		
		for (int i = 0; i <list.size(); i++) {
			String[] s = list.get(i);
			System.out.println(i);
			
			AllDataMethods.getParams(s);
			//System.out.println(getDescription(s));
			System.out.println(millage);
			//System.out.println(AllDataMethods.getParams(s));
			//System.out.println(getUserId(s));
			//System.out.println(getAdId(s));
			//System.out.println(getCategory(s));
			//System.out.println(getSubCategory(s));
			//System.out.println(make);
			//System.out.println(model);
			//System.out.println(make_year);
			//System.out.println(price);
			//System.out.println(currency_type);
			//System.out.println(price_type);
			//System.out.println(millage);
			//System.out.println(fuel_type);
			//System.out.println(optionalJson.toJSONString());
			System.out.println("------------------------------------------------------------------------");

		}

	}

	public static String make = "";
	public static String model = "";
	public static String make_year;
	public static String moto_hours;
	public static String price="vinay";
	public static String currency_type;
	public static String price_type;
	public static String millage;
	public static String fuel_type;
	static JSONObject optionalJson = new JSONObject();
	static JSONObject descriptionJson = new JSONObject();
	
	public static String getUserId(String[] s)
	{
		String user_id=s[1];
		return user_id;
		
	}

	public static String getAdId(String[] s) {
		String ad_id_from_site = s[2];
		return ad_id_from_site;

	}

	public static String getCategory(String[] s) {
		String category = s[5];
		return category;

	}

	public static String getSubCategory(String[] s) {
		String sub_category = s[7];
		return sub_category;
	}
	
	
	public static String getDescription(String[] s)
	{
		String details =s[19];
		JSONObject desc=new JSONObject();
		desc.put("en", details);
		String s1=descriptionJson.toJSONString();
		//System.out.println(s1);
		desc.put("extra_attributes", s1);
		String description=desc.toJSONString();
		return description;
		
	}
	public static String getOptionalField()
	{
		String optional_field=optionalJson.toJSONString();
		return optional_field;
	}
	public static String getImages(String[] s)
	{
		String img=s[22];
		String[] imgArray=img.split(";");
		String images="";
		for(int i=0; i<imgArray.length; i++)
		{
			if(i==imgArray.length-1)
			images+="https://ireland.apollo.olxcdn.com/v1/files/"+imgArray[i].trim()+"/image";
			else
				images+="https://ireland.apollo.olxcdn.com/v1/files/"+imgArray[i].trim()+"/image"+" <next> ";
			if(images.length()>6950)
			{
				break;
			}
		}
		return images;
	}
	public static String getParams(String[] s) {
		String params = s[21];
		String array[] = params.split("<br>");
		String key="";
		String value = "";
		
		for (int i = 0; i < array.length; i++) {
			String[] attribute = array[i].split("<=>");
			//System.out.println(attribute.length);
			key = attribute[0];
			try {
				value = attribute[1];
			} catch (Exception e) {
			}
			

			if (key.equals("make")) {
				make = value;
			}
			else if(key.equals("model"))
			{
				model=value;
			}
			else if(key.equals("year"))
			{
				make_year=value;
			}
			else if(key.equals("price"))
			{
				price=value;
			}
			else if(key.equals("price[currency]"))
			{
				currency_type=value;
			}
			else if(key.equals("fuel_type"))
			{
				fuel_type=value;
			}
			else if(key.equals("mileage"))
			{
				millage=value;
			} 
			else if(key.equals("lifetime"))
			{
				moto_hours=value;
			}
			else if(key.equals("rental_option"))
			{
				//price_type=key;
				if(value.equals("1"))
				{
					price_type="rent";
				}
				else
				{
					price_type="fixced";
				}
			}
			else if(key.equals("engine_power"))
			{
				key="Engine Power";
				optionalJson.put(key, value);
			}
			else if(key.equals("propulsion"))
			{
				key="undercarriage-type";
				optionalJson.put(key, value);
			}
			else if(key.equals("max_load_weight"))
			{
				key="maxPayload";
				optionalJson.put(key, value);
			}
			else if(key.equals("max_lifting_weight"))
			{
				key="maximumLiftCapacity";
				optionalJson.put(key, value);
			}
			else if(key.equals("max_lifting_height"))
			{
				key="maximumLiftHeight";
				optionalJson.put(key, value);
			}
			else if(key.equals("max_excavating_capacity"))
			{
				key="bucketCapacity";
				optionalJson.put(key, value);
			}
			else if(key.equals("max_excavating_depth"))
			{
				key="operating-depth";
				optionalJson.put(key, value);
			}
			else if(key.equals("vat"))
			{
				key="vatReclaimable";
				if(value.equals("1"))
				{
					value="yes";
				}
				else 
				{
					value="no";
				}
				optionalJson.put(key, value);
			}
			else if(key.equals("date_registration"))
			{
				key="first-registration-year";
				optionalJson.put(key, value);
			}
			else if(key.equals("engine_capacity"))
			{
				key="engineCapacityDisplacement";
				optionalJson.put(key, value);
			}
			else if(key.equals("pollution_standard"))
			{
				key="emissionLevel";
				optionalJson.put(key, value);
			}
			else if(key.equals("gearbox"))
			{
				key="transmission";
				optionalJson.put(key, value);
			}
			else if(key.equals("color"))
			{
				optionalJson.put(key, value);
			}
			else if(key.equals("wheel_axis"))
			{
				key="numberOfAxles";
				optionalJson.put(key, value);
			}
			else if(key.equals("max_cargo_weight"))
			{
				key="carrying-capacity";
				optionalJson.put(key, value);
			}
			else if(key.equals("max_weight"))
			{
				key="weight";
				optionalJson.put(key, value);
			}
			else if(key.equals("double_wheels"))
			{
				key="dual-rear-wheels";
				optionalJson.put(key, value);
			}
			else if(key.equals("original_owner"))
			{
				key="original-owner";
				optionalJson.put(key, value);
			}
			else if(key.equals("rhd"))
			{
				key="steeringWheelSide";
				optionalJson.put(key, value);
			}
			else if(key.equals("vin"))
			{
				key="serial";
				optionalJson.put(key, value);
			}
			else
			{
				descriptionJson.put(key, value);
			}
			
			
			
		}
		//System.out.println(optionalJson.toJSONString());
		//System.out.println(descriptionJson.toJSONString());
			/*for (String string : array) {
				System.out.println(string);
				
			}*/
		return params;

	}
	
	
	
public static void setter(String[] s, CSVPojo pojo_Object) {
		
		pojo_Object.setUser_id(getUserId(s));
		pojo_Object.setAd_id_from_site(getAdId(s));
		pojo_Object.setCategory(getCategory(s));
		pojo_Object.setSub_Category(getSubCategory(s));
		pojo_Object.setMake(make);
		pojo_Object.setModel(model);
		pojo_Object.setMake_Year(make_year);
		pojo_Object.setMillage(millage);
		pojo_Object.setFuel_type(fuel_type);
		pojo_Object.setMoto_hours(moto_hours);
		pojo_Object.setPrice(price);
		pojo_Object.setCurrency_Type(currency_type);
		pojo_Object.setPrice_type(price_type);
		pojo_Object.setImages(getImages(s));
		pojo_Object.setDescription(getDescription(s));
		pojo_Object.setOptional_fields(getOptionalField());
		pojo_Object.setSite_name("autovit.com");
		pojo_Object.setAd_status("imported");
	
	}
	
	public static void getter(PreparedStatement psmt, CSVPojo pojo_Object) throws SQLException {
		int i = 2;
		psmt.setString(1, pojo_Object.getUser_id());
		psmt.setString(i++, pojo_Object.getAd_id_from_site());
		psmt.setString(i++, pojo_Object.getCategory());
		psmt.setString(i++, pojo_Object.getSub_Category());
		psmt.setString(i++, pojo_Object.getMake());
		psmt.setString(i++, pojo_Object.getModel());
		psmt.setString(i++, pojo_Object.getMake_Year());
		psmt.setString(i++, pojo_Object.getMillage());
		psmt.setString(i++, pojo_Object.getFuel_type());
		psmt.setString(i++, pojo_Object.getMoto_hours());
		psmt.setString(i++, pojo_Object.getPrice());
		psmt.setString(i++, pojo_Object.getCurrency_Type());
		psmt.setString(i++, pojo_Object.getPrice_type());
		psmt.setString(i++, pojo_Object.getImages());
		psmt.setString(i++, pojo_Object.getDescription());
		psmt.setString(i++, pojo_Object.getOptional_fields());
		psmt.setString(i++, pojo_Object.getSite_name());
		psmt.setString(i++, pojo_Object.getAd_status());
	
	}
	

	
	

}
