package csvProject;

public class CSVPojo {
	private String user_id;
	private String Ad_id_from_site;
	private String category;
	private String sub_Category;
	private String make;
	private String model;
	private String make_Year;
	private String millage;
	private String fuel_type;
	private String moto_hours;
	private String price;
	private String currency_Type;
	private String price_type;
	private String images;
	private String description;
	private String optional_fields;
	private String site_name;
	private String ad_status;
	
	public CSVPojo() {
		
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getAd_id_from_site() {
		return Ad_id_from_site;
	}

	public void setAd_id_from_site(String ad_id_from_site) {
		Ad_id_from_site = ad_id_from_site;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSub_Category() {
		return sub_Category;
	}

	public void setSub_Category(String sub_Category) {
		this.sub_Category = sub_Category;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getMake_Year() {
		return make_Year;
	}

	public void setMake_Year(String make_Year) {
		this.make_Year = make_Year;
	}

	public String getMillage() {
		return millage;
	}

	public void setMillage(String millage) {
		this.millage = millage;
	}

	public String getFuel_type() {
		return fuel_type;
	}

	public void setFuel_type(String fuel_type) {
		this.fuel_type = fuel_type;
	}

	public String getMoto_hours() {
		return moto_hours;
	}

	public void setMoto_hours(String moto_hours) {
		this.moto_hours = moto_hours;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getCurrency_Type() {
		return currency_Type;
	}

	public void setCurrency_Type(String currency_Type) {
		this.currency_Type = currency_Type;
	}

	public String getPrice_type() {
		return price_type;
	}

	public void setPrice_type(String price_type) {
		this.price_type = price_type;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOptional_fields() {
		return optional_fields;
	}

	public void setOptional_fields(String optional_fields) {
		this.optional_fields = optional_fields;
	}

	public String getSite_name() {
		return site_name;
	}

	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	public String getAd_status() {
		return ad_status;
	}

	public void setAd_status(String ad_status) {
		this.ad_status = ad_status;
	}
	
}
