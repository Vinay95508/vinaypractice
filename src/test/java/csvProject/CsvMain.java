package csvProject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;



public class CsvMain {

	
	public static void main(String[] args) throws IOException, ParseException, SQLException, ClassNotFoundException {
		FTPClient ftp = new FTPClient();
		ftp.connect("54.229.200.159");
		ftp.login("Autovit.ro", "autovit9123");
		ftp.enterLocalPassiveMode();
		ftp.setFileType(FTP.BINARY_FILE_TYPE);
		LocalDate now = LocalDate.now().minusDays(1); 
		String date=""+now.getYear()+now.getMonthValue()+(now.getDayOfMonth());
		System.out.println(date);

		String Path = date+" - AVT - AVT Tradus ads export.csv";
		String file="C:\\Users\\K Vinay\\Desktop\\Json files\\"+date+" - AVT - AVT Tradus ads export.csv";
		FileOutputStream fos = new FileOutputStream(new File(file));
		FileReader fileReader = new FileReader(file);
		ftp.retrieveFile(Path, fos);
		CSVReader reader = new CSVReaderBuilder(fileReader).withSkipLines(1).build();
		List<String[]> list = reader.readAll();
		
		DatabaseMethods.getDataBaseConnection();
		DatabaseMethods.getDataBaseDriver();
		PreparedStatement psmt = DatabaseMethods.getPreparedStatement();
		CSVPojo pojo_Object= new CSVPojo();
		for(int i=0; i<list.size(); i++)
		{
			System.out.println(i);
			String[] s=list.get(i);
			AllDataMethods.getParams(s);
			System.out.println(AllDataMethods.millage);
			AllDataMethods.setter(s, pojo_Object);
			AllDataMethods.getter(psmt, pojo_Object);
			DatabaseMethods.insertValues(psmt);
			
		}
		ftp.logout();
		ftp.disconnect();
		
		
		
		
		

	}

}
