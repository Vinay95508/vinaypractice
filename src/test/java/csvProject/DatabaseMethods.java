package csvProject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class DatabaseMethods {
	static String insertion = "INSERT INTO `scraping`.`ad_details` (`user_id`, `Ad_id_From_Site`, `Category`, `Sub_Category`, `Make`, `Model`, `Make_Year`, `Mileage`, `Fuel_Type`, `Moto_hours`, `Price`, `Currency_Type`, `price_type`, `Images`, `Description`, `optional_fields`, `Site_Name`, `Ad_Status`) VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"; 
			
	static PreparedStatement psmt = null;
	static Connection con = null;

	public static void getDataBaseDriver() throws ClassNotFoundException {
		Class.forName("com.mysql.cj.jdbc.Driver");
	}

	public static Connection getDataBaseConnection() throws SQLException {
		return con = DriverManager.getConnection("jdbc:mysql://localhost:3306?user=root&password=root");
	}

	public static PreparedStatement getPreparedStatement() throws SQLException {
		if (psmt == null) {
			psmt = con.prepareStatement(insertion);
		}
		return psmt;
	}

	public static void insertValues(PreparedStatement psmt, List listOfTableContent) throws SQLException {
		for (int i = 1; i <= listOfTableContent.size(); i++) {
			if (listOfTableContent.get(i - 1) instanceof Integer) {
				int autonum = (Integer) listOfTableContent.get(i - 1);
				psmt.setInt(i, autonum);
			} else if (listOfTableContent.get(i - 1) instanceof String) {
				String data = (String) listOfTableContent.get(i - 1);
				psmt.setString(i, data);
			}

		}
		psmt.executeUpdate();
	}

	public static void insertValues(PreparedStatement psmt) throws SQLException {
		psmt.executeUpdate();
	}

	public static void close() throws SQLException {
		if (psmt != null) {
			psmt.close();
		}
		if (con != null) {
			con.close();
		}

	}

}
